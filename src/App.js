import logo from './logo.svg';
import './App.css';
import TeamScoreboard from './Components/TeamScoreboard';
import Header from './Components/Header';
import AppTitle from './Components/AppTitle';

function App() {
  const content = [{
    name: "Equipo 1",
    score: 10,
    activities: [{
      name: "tarea 1",
      score: 5
    },
    {
      name: "tarea 2",
      score: 5
    },
    {
      name: "tarea 3",
      score: 0
    }]
  },{
    name: "Equipo 2",
    score: 20,
    activities: [{
      name: "tarea 1",
      score: 5
    },
    {
      name: "tarea 2",
      score: 5
    },
    {
      name: "tarea 3",
      score: 10
    }]
  },{
    name: "Equipo 3",
    score: 20,
    activities: [{
      name: "tarea 1",
      score: 15
    },
    {
      name: "tarea 2",
      score: 5
    },
    {
      name: "tarea 3",
      score: 0
    }]
  },
  {
    name: "Equipo 4",
    score: 20,
    activities: [{
      name: "tarea 1",
      score: 15
    },
    {
      name: "tarea 2",
      score: 5
    },
    {
      name: "tarea 3",
      score: 0
    }]
  },
  {
    name: "Equipo 5",
    score: 20,
    activities: [{
      name: "tarea 1",
      score: 15
    },
    {
      name: "tarea 2",
      score: 5
    },
    {
      name: "tarea 3",
      score: 0
    }]
  }]

  const title = "SOLERABOARD"

  return (
    <div className="App">
      <header>
        {/* <img src={logo} className="App-logo" alt="logo" /> */}
        <Header></Header>
      </header>
      <main>
        <AppTitle title={title}></AppTitle>
       
        <TeamScoreboard content={content}> {content} </TeamScoreboard>
      </main>
    </div>
  );
}

export default App;
