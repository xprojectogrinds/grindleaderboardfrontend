import { createTheme } from '@mui/material/styles';

const theme = createTheme({
    breakpoints: {
       values: {
        xs: 0,
        sm: 450,
        md: 1024,
        lg: 1024,
        xl: 2000
       }
    },
    palette: {
        primary: {
          main: '#FFF',
        },
      },
      components: {
        Header: {
            variants: [
                {
                    props: {variant: 'small'}
                },
                {
                    style: {
                        background: '#9525f7'
                    }
                },
                {
                    props: {variant: 'medium'}
                },
                {
                    style: {
                        background: '#9aaaf7'
                    }
                }
            ]
        }
       }
  });


  export default theme;