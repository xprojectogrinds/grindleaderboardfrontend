import React from 'react'
import Slide from '@mui/material/Slide';
import TeamScoreDialog from './TeamScoreDialog';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';


  const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="right" ref={ref} {...props} />;
  });

  

  
  export default function TeamScoreboard({item}) {
    const [open, setOpen] = React.useState(false);
    const theme = useTheme();

    const small = useMediaQuery(theme.breakpoints.down('sm'))
  // const medium = useMediaQuery(theme.breakpoints.down('md'))

    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

  return small ? 
  (
    <div>
      <div className="card-small" onClick={handleClickOpen} style={{color: theme.palette.primary.main}}> {item.name}: {`color ${theme.palette.primary.main}`} {item.score} </div>
      <TeamScoreDialog open={open} handleClose={handleClose} Transition={Transition} item={item}></TeamScoreDialog>
    </div>
  )
  :
  (
    <div>
      <div className="card" onClick={handleClickOpen}> {item.name}: {`color ${theme.palette.primary.main}`} {item.score} </div>
      <TeamScoreDialog open={open} handleClose={handleClose} Transition={Transition} item={item}></TeamScoreDialog>
    </div>
  )
}

