import React from "react";
import TeamScoreboardCard from './TeamScoreboardCard';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';




function TeamScoreboard (props) {

    const theme = useTheme();

    const small = useMediaQuery(theme.breakpoints.down('sm'))

    return (
    <div className={small ? "flex-column" : "flex"} style={{paddingBottom: "20px"}}>
        {props.content.map((item) => <TeamScoreboardCard item={item} key={item.name}></TeamScoreboardCard>)}
    </div>
    )
}

export default TeamScoreboard