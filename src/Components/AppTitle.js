import React from "react";

export default function AppTitle(props) {
    return (
        <div>{props.title}</div>
    )
}