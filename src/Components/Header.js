import { useTheme } from '@mui/material/styles';
import React from 'react';
import useMediaQuery from '@mui/material/useMediaQuery';
import logoSolera from "../logo-solera.svg"

function Header () {

    const theme = useTheme()

    const small = useMediaQuery(theme.breakpoints.down('sm'))
    const medium = useMediaQuery(theme.breakpoints.down('md'))

    return small ? (
        <div className="header-small">
            <img src={logoSolera} alt='logo of the solera company' width="150px"></img>
        </div>
    ) : medium ? (
        <div className='header'>Medium header</div>
    ) : (
        <div className='header'>Large header</div>
    )

}

export default Header