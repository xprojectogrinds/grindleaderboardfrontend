import React from 'react'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

function TeamScoreDialog({item, open, handleClose, Transition}) {

  return (
    <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
        
        >
        <DialogTitle>{`${item.name}  - Team Score: ${item.score}`}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            
            {/* Team Score = {item.score}
            Componente X */}
            { item.activities.map((activity => <div className="activityScoreListCard"> <span>{activity.name.slice(0,1).toUpperCase()}{activity.name.slice(1)}</span> <span>{activity.score} puntos</span> </div>)) }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        </DialogActions>
      </Dialog>
  )
}

export default TeamScoreDialog


